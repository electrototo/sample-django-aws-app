Parameters:
  InstancesKeyPair:
    Type: 'AWS::EC2::KeyPair::KeyName'
    Description: La llave que se va a utilizar para configurar las instancias de EC2

Resources:
  WebServerSG:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Permite conectividad al puerto 80 y 443
      SecurityGroupIngress:
        - CidrIp: '0.0.0.0/0'
          FromPort: 80
          ToPort: 80
          IpProtocol: tcp
        - CidrIp: '0.0.0.0/0'
          FromPort: 443
          ToPort: 443
          IpProtocol: tcp

  SSHSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      GroupDescription: Permite conectividad al puerto 22
      SecurityGroupIngress:
        - CidrIp: '0.0.0.0/0'
          FromPort: 22
          ToPort: 22
          IpProtocol: tcp

  DjangoInstanceRole:
    Type: 'AWS::IAM::Role'
    Properties:
      Description: 'Rol para que la instancia de ec2 pueda hacer uso de codedeploy agent'
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: 'Allow'
            Principal:
              Service: 'ec2.amazonaws.com'
            Action: 'sts:AssumeRole'
      Path: '/'
      Policies:
        - PolicyName: root
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Effect: 'Allow'
                Action:
                  - 's3:Get*'
                  - 's3:List*'
                Resource: '*'

  CodeDeployRole:
    Type: 'AWS::IAM::Role'
    Properties:
      Description: 'Rol para que codedeploy pueda hacer despliegues en ec2'
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: 'Allow'
            Principal:
              Service: 'codedeploy.amazonaws.com'
            Action: 'sts:AssumeRole'
      Path: '/'
      Policies:
        - PolicyName: root
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Effect: 'Allow'
                Action:
                  - 'autoscaling:*'
                  - 'ec2:DescribeInstances'
                  - 'ec2:DescribeInstanceStatus'
                  - 'ec2:TerminateInstances'
                  - 'tag:GetResources'
                  - 'sns:Publish'
                  - 'cloudwatch:DescribeAlarms'
                  - 'cloudwatch:PutMetricAlarm'
                  - 'elasticloadbalancing:*'
                Resource: '*'

  DjangoInstanceIam:
    Type: 'AWS::IAM::InstanceProfile'
    Properties:
      Roles:
        - !Ref DjangoInstanceRole

  DjangoInstance:
    Type: 'AWS::EC2::Instance'
    Properties:
      IamInstanceProfile: !Ref DjangoInstanceIam
      ImageId: ami-04505e74c0741db8d
      InstanceType: t2.micro
      KeyName:
        Ref: InstancesKeyPair
      SecurityGroups:
        - !Ref WebServerSG
        - !Ref SSHSecurityGroup
      UserData:
        'Fn::Base64':
          !Sub |
            #!/bin/bash -xe
            sudo apt update
            sudo apt install -y ruby-full wget
            cd /home/ubuntu/
            wget https://aws-codedeploy-${AWS::Region}.s3.${AWS::Region}.amazonaws.com/latest/install
            chmod +x ./install
            ./install auto
            mkdir -p /opt/aws/bin
            wget https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-py3-latest.tar.gz
            python3 -m easy_install --script-dir /opt/aws/bin aws-cfn-bootstrap-py3-latest.tar.gz
            /opt/aws/bin/cfn-init -v -s ${AWS::StackId} -r DjangoInstance --region ${AWS::Region} || error_exit 'Error al correr cfn-init'
      Tags:
        - Key: 'Environment'
          Value: 'Production'
    Metadata:
      AWS::CloudFormation::Init:
        config:
          packages:
            apt:
              nginx: []
              python3-pip: []
          files:
            '/etc/nginx/sites-available/django-sample-project':
              content: |
                server {
                  listen 80;

                  location = /favicon.ico { access_log off; log_not_found off; }
                  location / {
                    proxy_set_header Host $http_host;
                    proxy_set_header X-Real-IP $remote_addr;
                    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                    proxy_set_header X-Forwarded-Proto $scheme;

                    proxy_pass http://127.0.0.1:8000;
                  }
                }
              mode: '000644'
              owner: 'root'
              group: 'root'
            '/etc/systemd/system/gunicorn.service':
              content: |
                [Unit]
                Description=gunicorn daemon
                After=network.target

                [Service]
                User=ubuntu
                Group=www-data
                WorkingDirectory=/home/ubuntu/sample-django-application/sample_project/
                ExecStart=/usr/bin/gunicorn --workers 3 -b http://127.0.0.1:8000 sample_project.wsgi:application

                [Install]
                WantedBy=multi-user.target
              mode: '000644'
              owner: 'root'
              group: 'root'
          commands:
            installgunicorn:
              command: 'pip3 install gunicorn'
            linknginxconfig:
              command: 'ln -s /etc/nginx/sites-available/django-sample-project /etc/nginx/sites-enabled'
          services:
            systemd:
              nginx:
                ensureRunning: true
                enabled: true
              gunicorn:
                ensureRunning: true
                enabled: true

  CodeDeployApplication:
    Type: 'AWS::CodeDeploy::Application'
    Properties:
      ApplicationName: 'aplicacion-django'
      ComputePlatform: Server

  DjangoDeploymentGroup:
    Type: 'AWS::CodeDeploy::DeploymentGroup'
    Properties:
      ApplicationName: !Ref CodeDeployApplication
      ServiceRoleArn: !GetAtt CodeDeployRole.Arn
      DeploymentConfigName: 'CodeDeployDefault.OneAtATime'
      Ec2TagFilters:
        - Key: 'Environment'
          Type: 'KEY_AND_VALUE'
          Value: 'Production'

Outputs:
  DjangoInstanceIp:
    Description: La IP de la instancia de django
    Value: !GetAtt DjangoInstance.PublicIp
